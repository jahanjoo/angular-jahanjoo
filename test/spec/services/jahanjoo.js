/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

describe('Service: $jahanjoo', function() {
	var originalTimeout;
	var $rootScope;
	var $jahanjoo;
	var $timeout;
	var PaginatorParameter;
	var $httpBackend;

	beforeEach(function() {
		// load the module
		module('pluf.jahanjoo');
		// Initialize the controller and a mock scope
		inject(function(_$jahanjoo_, _$rootScope_, _$httpBackend_, _$timeout_,
				_PaginatorParameter_) {
			$jahanjoo = _$jahanjoo_;
			$rootScope = _$rootScope_;
			$httpBackend = _$httpBackend_;
			$timeout = _$timeout_;
			PaginatorParameter = _PaginatorParameter_;
		});

		originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
		jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
	});

	afterEach(function() {
		jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
	});

	it('should contains location function', function() {
		expect(angular.isFunction($jahanjoo.location)).toBe(true);
	});
	it('should contains locations function', function() {
		expect(angular.isFunction($jahanjoo.locations)).toBe(true);
	});

	it('should contains newLocation function', function() {
		expect(angular.isFunction($jahanjoo.newLocation)).toBe(true);
	});
	it('should call /api/jayab/location/new to create new location', function(
			done) {
		var id = 1;
		var data = {
			id : id,
			title : 'title',
			latitude : 25.5,
			longitude : 25.5
		};
		$jahanjoo.newLocation(data)//
		.then(function(object) {
			expect(object).not.toBeNull();
			expect(object.id).not.toBeUndefined();
			expect(object.id).toBe(id);
			done();
		});

		$httpBackend//
		.expect('POST', '/api/jayab/location/new')//
		.respond(200, data);
		expect($httpBackend.flush).not.toThrow();
		$rootScope.$apply();
	});

	it('should contains tag function', function() {
		expect(angular.isFunction($jahanjoo.tag)).toBe(true);
	});

	it('should contains tags function', function() {
		expect(angular.isFunction($jahanjoo.tags)).toBe(true);
	});

	it('should contains newTag function', function() {
		expect(angular.isFunction($jahanjoo.newTag)).toBe(true);
	});
});
