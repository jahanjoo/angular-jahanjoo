/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('pluf.jahanjoo')

/**
 * ساختار داده‌ای یک مکان را ایجاد می‌کند. علاوه بر این ابزارهای اولیه مورد نیاز
 * برای دستکاری مکان را نیز در اختیار می‌گذارد
 */
.factory('PLocation', function($http, PObject) {
	/**
	 * یک نمونه جدید از این کلاس ایجاد می‌کند.
	 */
	var pLocation = function() {
		PObject.apply(this, arguments);
	};
	pLocation.prototype = new PObject();

	/**
	 * این مکان را از سیستم حذف می‌کند.
	 */
	pLocation.prototype.remove = function() {
		var scope = this;
		return $http({
			method : 'DELETE',
			url : '/api/jayab/location/' + this.id,
		})//
		.then(function(res) {
			scope.setData(res);
			scope.id = 0;
			return scope;
		});
	};

	/**
	 * خصوصیت‌های مکان را به روز می‌کند.
	 * 
	 */
	pLocation.prototype.update = function() {
		// TODO: maso, 1395: پیاده سازی
	};

	/**
	 * دسترسی به یک تگ خاص
	 */
	pLocation.prototype.tag = function() {
		// TODO: maso, 1395: پیاده سازی
	};

	/**
	 * فهرست تمام تگ‌های یک مکان
	 */
	pLocation.prototype.tags = function() {
		// TODO: maso, 1395: پیاده سازی
	};

	/**
	 * اضافه کردن یک تگ چدید
	 */
	pLocation.prototype.newTag = function() {
		// TODO: maso, 1395: پیاده سازی
	};

	// returns module
	return pLocation;
});
